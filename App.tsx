import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import Navigator from 'navigator/Navigator';
import store from 'src/redux';
import { init } from 'src/localization';
import { SafeAreaView } from 'react-native-safe-area-context';
import { fb } from 'src/api/firebase';
import { LogBox } from 'react-native';
import _ from 'lodash';
import ExpoStatusBar from 'expo-status-bar/build/ExpoStatusBar';

LogBox.ignoreLogs(['Setting a timer']);
const _console = _.clone(console);
console.warn = (message: string) => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

export const App = () => {
  init();
  useEffect(() => {
    fb.auth().onAuthStateChanged((user) => {
      if (user != null) {
        user.getIdToken();
      }
    });
  }, []);

  return (
    <SafeAreaView edges={['right', 'left']} style={{ flex: 1 }}>
      <Provider store={store}>
        <Navigator />
      </Provider>
      <ExpoStatusBar style={'light'} />
    </SafeAreaView>
  );
};

export default App;
