import React from 'react';
import App from '../App';
import { render, waitFor } from '@testing-library/react-native';

describe('<App />', () => {
  it('matches snapshot', async () => {
    const tree = render(<App />).toJSON();

    await waitFor(() => {
      expect(tree).toMatchSnapshot();
    });
  });
});
