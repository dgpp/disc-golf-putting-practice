import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import EmailPwForm from 'src/components/emailpwform/EmailPwForm';

describe('Email and password form', () => {
  it('', async () => {
    const mockSubmit = jest.fn();
    const { getByTestId } = render(<EmailPwForm handleSubmit={mockSubmit} />);

    const emailInput = getByTestId('email');
    const pwInput = getByTestId('pw');
    const submitButton = getByTestId('submit');

    fireEvent.changeText(emailInput, 'dgpp-tester@dgpp.fi');
    fireEvent.changeText(pwInput, 'dgpp-TestGuy!');
    fireEvent.press(submitButton);

    await waitFor(() => {
      expect(mockSubmit).toHaveBeenCalled();
    });
  });
});
