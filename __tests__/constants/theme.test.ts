import { getScoreColor, themePalette } from 'constants/theme';

const mockValBad = 20;
const mockValPoor = 40;
const mockValAverage = 80;
const mockValGood = 90;

describe('score colors: ', () => {
  test('bad score should return red', () => {
    expect(getScoreColor(mockValBad)).toBe(themePalette.percentageBad);
  });
  test('poor score should return orange', () => {
    expect(getScoreColor(mockValPoor)).toBe(themePalette.percentagePoor);
  });
  test('average score should return yellow', () => {
    expect(getScoreColor(mockValAverage)).toBe(themePalette.percentageAvg);
  });
  test('good score should return green', () => {
    expect(getScoreColor(mockValGood)).toBe(themePalette.percentageGood);
  });
});
