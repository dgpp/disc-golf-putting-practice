import React from 'react';
import AuthLoadingScreen from 'src/features/authentication/AuthLoadingScreen';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { render } from '@testing-library/react-native';

describe('Authentication loading screen', () => {
  it('matches snapshot', () => {
    const props: any = {
      navigation: {},
      route: {},
    };
    const tree = render(
      <Provider store={store}>
        <AuthLoadingScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
