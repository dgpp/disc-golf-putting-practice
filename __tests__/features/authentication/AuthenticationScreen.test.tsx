import React from 'react';
import AuthenticationScreen from 'src/features/authentication/AuthenticationScreen';
import { fireEvent, render, waitFor } from '@testing-library/react-native';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { Screens } from 'src/constants/screens';
import { LogInStatuses } from 'src/models/auth.model';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

describe('Authentication screen', () => {
  it('matches snapshot', () => {
    const props: any = {
      navigation: {},
      route: {},
    };
    const tree = render(
      <Provider store={store}>
        <AuthenticationScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('navigates to login screen from login button', async () => {
    const mockFn = jest.fn((screen: Screens) => {
      return null;
    });

    const mockStore: any = configureMockStore([thunk]);
    const _store = mockStore({
      auth: {
        logInStatus: LogInStatuses.LoggedOut,
        userId: '',
      },
    });

    const props: any = {
      navigation: { navigate: mockFn },
      route: {},
    };

    const { getByTestId } = render(
      <Provider store={_store}>
        <AuthenticationScreen {...props} />
      </Provider>
    );

    const loginButton = getByTestId('login');
    const createNewUserButton = getByTestId('create-user');

    fireEvent.press(loginButton);
    fireEvent.press(createNewUserButton);

    await waitFor(async () => {
      expect(mockFn).toHaveBeenCalledWith(Screens.logIn);
    });
  });
});
