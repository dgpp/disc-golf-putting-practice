import React from 'react';
import LogInScreen from 'src/features/authentication/LogInScreen';
import { Provider } from 'react-redux';
import store from '../../../src/redux';

import { render } from '@testing-library/react-native';

describe('login Screen snapshot', () => {
  it('when backbutton pressed navigate is', async () => {
    const navigation: any = {};
    const route: any = {};
    const props: any = { navigation, route };
    const tree = render(
      <Provider store={store}>
        <LogInScreen {...props} />)
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
