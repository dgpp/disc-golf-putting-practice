import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';

import { Provider } from 'react-redux';
import store from '../../../src/redux';

import SignInScreen from 'src/features/authentication/SignInScreen';

describe('Sign In screen tests', () => {
  it('snapshot test', () => {
    const props: any = {
      navigation: {},
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <SignInScreen {...props} />)
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
