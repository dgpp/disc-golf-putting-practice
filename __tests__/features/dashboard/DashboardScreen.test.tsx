import React from 'react';
import DashboardScreen from 'src/features/dashboard/DashboardScreen';
import { Provider } from 'react-redux';
import store from '../../../src/redux';
import { render } from '@testing-library/react-native';

describe('Dashboard screen', () => {
  it('matches snapshot', async () => {
    const tree = render(
      <Provider store={store}>
        <DashboardScreen />
      </Provider>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
