import { PuttingStyles, PuttModel, WindDirections } from 'src/models';
import {
  calculateForStyle,
  calculateOverallPercentage,
} from '../../../src/features/dashboard/calcFunctions';

const mockPutts: PuttModel[] = [
  {
    id: 0,
    hit: true,
    puttingStyle: PuttingStyles.JumpPutt,
    range: 20,
    wind: 0,
    windDirection: WindDirections.HeadWind,
  },
  {
    id: 1,
    hit: false,
    puttingStyle: PuttingStyles.JumpPutt,
    range: 20,
    wind: 0,
    windDirection: WindDirections.HeadWind,
  },
  {
    id: 2,
    hit: true,
    puttingStyle: PuttingStyles.Staggered,
    range: 20,
    wind: 0,
    windDirection: WindDirections.HeadWind,
  },
];

// test that it filters by style correctly
describe('calculateforStyle function', () => {
  it('filters by style', () => {
    const res = calculateForStyle(mockPutts, PuttingStyles.JumpPutt);
    expect(res).toBe(0.5);
  });
});

describe('calculateOverallPercentage', () => {
  it('returns a number between 0 - 1', () => {
    const res = calculateOverallPercentage(mockPutts);
    const betweenZeroAndHundred = res >= 0 && res <= 1;
    expect(betweenZeroAndHundred).toBeTruthy();
  });
});
