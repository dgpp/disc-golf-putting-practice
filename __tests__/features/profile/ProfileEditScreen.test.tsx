import React from 'react';
import ProfileEditScreen from 'src/features/profile/ProfileEditScreen';
import { Provider } from 'react-redux';
import store from '../../../src/redux';
import { render } from '@testing-library/react-native';

describe('Profile edit screen', () => {
  it('matches snapshot', async () => {
    const props: any = {
      navigation: { addListener: jest.fn() },
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <ProfileEditScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
