import React from 'react';
import ProfileScreen from 'src/features/profile/ProfileScreen';
import { Provider } from 'react-redux';
import store from '../../../src/redux';
import { render } from '@testing-library/react-native';

describe('Profile screen', () => {
  it('matches snapshot', async () => {
    const props: any = {
      navigation: {},
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <ProfileScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
