import React from 'react';
import PuttDetailsScreen from 'src/features/training/PuttDetailsScreen';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { render } from '@testing-library/react-native';

describe('Profile edit screen', () => {
  it('matches snapshot', () => {
    const props: any = {
      navigation: { addListener: jest.fn() },
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <PuttDetailsScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
