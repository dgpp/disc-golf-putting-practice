import React from 'react';
import PuttListScreen from 'src/features/training/PuttListScreen';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { render } from '@testing-library/react-native';

describe('Putt list screen', () => {
  it('matches snapshot', () => {
    const props: any = {
      navigation: {},
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <PuttListScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
