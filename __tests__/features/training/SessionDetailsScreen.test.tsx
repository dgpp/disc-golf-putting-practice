import React from 'react';
import SessionDetailsScreen from 'src/features/training/SessionDetailsScreen';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { render } from '@testing-library/react-native';

describe('Profile edit screen', () => {
  it('matches snapshot', () => {
    const props: any = { navigation: { addListener: jest.fn() }, route: {} };

    const tree = render(
      <Provider store={store}>
        <SessionDetailsScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
