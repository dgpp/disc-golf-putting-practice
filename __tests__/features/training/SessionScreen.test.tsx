import React from 'react';
import SessionScreen from 'src/features/training/SessionScreen';
import { Provider } from 'react-redux';
import store from 'src/redux';
import { render } from '@testing-library/react-native';

describe('Session screen', () => {
  it('matches snapshot', () => {
    const props: any = {
      navigation: { addListener: jest.fn() },
      route: {},
    };

    const tree = render(
      <Provider store={store}>
        <SessionScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
