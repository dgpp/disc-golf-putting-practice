import React from 'react';
import TrainingListScreen from 'src/features/training/TrainingListScreen';
import { Provider } from 'react-redux';
import store from '../../../src/redux';
import { render } from '@testing-library/react-native';

describe('Training list screen', () => {
  it('matches snapshot', () => {
    const props: any = { navigation: {}, route: {} };

    const tree = render(
      <Provider store={store}>
        <TrainingListScreen {...props} />
      </Provider>
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
