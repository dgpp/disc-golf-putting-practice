module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    "plugins": [
      'babel-plugin-transform-inline-environment-variables',
      [
        "module-resolver",
        {
          "alias": {
            "src": "./src",
            "compoments": "./src/components",
            "constants": "./src/constants",
            "navigator": "./src/navigator",
            "features": "./src/features",
            "utils": "./src/utils",
          }
        }
      ]
    ]
  };
};
