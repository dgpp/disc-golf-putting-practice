import * as SecureStore from 'expo-secure-store';
import { fb } from 'src/api/firebase';
import { AuthErrorModel, UserDataSchema } from 'src/models';

export const setLocalUserInformation = async (
  email: string,
  password: string
) => {
  SecureStore.setItemAsync('email', email);
  SecureStore.setItemAsync('pw', password);
};

export const getLocalUserInformation = async (): Promise<any> => {
  const email = await SecureStore.getItemAsync('email');
  const password = await SecureStore.getItemAsync('pw');

  if (email && password) {
    return { email, password };
  }

  return { email: '', password: '' };
};

/**
 *
 * @param email
 * @param password
 */
export const signInByMail = async (
  email: string,
  password: string,
  errorHandler?: (error: AuthErrorModel) => any
): Promise<firebase.auth.UserCredential | undefined> => {
  try {
    const credentials = await fb
      .auth()
      .signInWithEmailAndPassword(email, password);

    if (credentials) {
      setLocalUserInformation(email, password);
      return credentials;
    }
  } catch (error) {
    console.log(error);
    if (errorHandler) errorHandler(error);
  }
};

export const signOut = async () => {
  setLocalUserInformation('', '');
  fb.auth()
    .signOut()
    .then(() => {
      return true;
    })
    .catch((err) => {
      console.log(err);
      return false;
    });
};

export const signInWithStoredUser = async (): Promise<
  firebase.auth.UserCredential | undefined
> => {
  try {
    const userInfo = await getLocalUserInformation();
    if (userInfo) {
      return signInByMail(userInfo.email, userInfo.password);
    }
  } catch (error) {
    console.log(error);
  }
};

/**
 *
 * @param email
 * @param password
 */
export const createUserWithEmail = async (
  email: string,
  password: string,
  errorHandler: (err: AuthErrorModel) => any
): Promise<firebase.auth.UserCredential | undefined> => {
  try {
    const resp = await fb
      .auth()
      .createUserWithEmailAndPassword(email, password);
    if (resp && resp.user) {
      await fb
        .database()
        .ref('users/' + resp.user.uid)
        .set({
          email,
          displayName: email.split('@')[0] || '',
        } as UserDataSchema);

      console.log('setting displayname', email.split('@')[0]);

      return resp;
    }
  } catch (error) {
    if (errorHandler) errorHandler(error);
  }
};
