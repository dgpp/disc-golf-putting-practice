import { FirebaseError } from 'firebase';
import {
  PuttingStyles,
  TrainingSessionModel,
  UserDataSchema,
} from 'src/models';
import { fb } from './firebase';

export const readUserData = async (
  userId: string
): Promise<UserDataSchema | null> => {
  try {
    const snap: UserDataSchema = await (
      await fb
        .database()
        .ref('users/' + userId)
        .once('value')
    ).val();

    return snap ? snap : null;
  } catch (error) {
    console.log(error);
  }

  return null;
};

export const readSessions = async (
  userId: string
): Promise<TrainingSessionModel[]> => {
  try {
    const snapshot = await fb
      .database()
      .ref('users/' + userId + '/sessions/')
      .once('value');

    if (snapshot) {
      const sessions = formatToTrainingSessionModel(snapshot);
      return sessions;
    }
  } catch (error) {
    console.log(error);
  }
  return [];
};

const formatToTrainingSessionModel = (
  snapshot: firebase.database.DataSnapshot
): TrainingSessionModel[] => {
  const sessionsObject = snapshot.val();
  if (Array.isArray(sessionsObject)) {
    const sessions = [...sessionsObject];
    return sessions;
  }
  return [];
};

export const saveSessions = (
  userId: string,
  sessions: TrainingSessionModel[]
) => {
  fb.database()
    .ref('users/' + userId + '/sessions/')
    .set(sessions);
};

// export const readDisplayName = await (userId) => {
//   const snap = await fb.database().ref('users/' + userId + '/displayName')
// }

// export const setDefaultPuttingStyle = (
//   userId: string,
//   style: PuttingStyles
// ) => {
//   fb.database()
//     .ref('users/' + userId)
//     .set({ defaultPuttingStyle: style });
// };
