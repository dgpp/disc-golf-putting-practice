import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyD0ozdgEJMdDp6hxxGGNOSlr4pzyI8goEI",
  authDomain: "dgpp-3f275.firebaseapp.com",
  databaseURL: "https://dgpp-3f275.firebaseio.com/",
  projectId: "dgpp-3f275",
}

export const initFirebase = () => {
  try {
    firebase.initializeApp(firebaseConfig);
  } catch (err) {
    if (!/already exists/.test(err.message)) {
      console.error('Firebase initialization error', err.stack);
    }
  }
  const fb = firebase;
  return fb;
};

export const fb = initFirebase()
