import { fb } from './firebase';

export const updateDisplayName = async (
  userId: string,
  newName: string
): Promise<boolean> => {
  try {
    const save = fb
      .database()
      .ref('users/' + userId + '/displayName/')
      .set(newName);
    return save ? true : false;
  } catch (error) {
    console.log(error);
  }

  return false;
};
