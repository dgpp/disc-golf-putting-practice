import React, { FunctionComponent, useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon, Input } from 'react-native-elements';
import { t } from 'src/localization';
import { AuthErrorModel } from 'src/models';

interface EmailPwFormProps {
  handleSubmit: (email: string, pw: string, errorHandler: (err: any) => void) => Promise<any>;
  loginForm?: boolean;
}

const EmailPwForm: FunctionComponent<EmailPwFormProps> = (props) => {
  const [passwordInput, setPasswordInput] = useState<string>('');
  const [passwordErrorMessage, setPasswordErrorMessage] = useState<string>('');
  const [emailInput, setEmailInput] = useState<string>('');
  const [emailErrorMessage, setEmailErrorMessage] = useState<string>('');
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  useEffect(() => {
    emailErrorMessage.length > 0 && setEmailErrorMessage('');
    passwordErrorMessage.length > 0 && setPasswordErrorMessage('');
  }, [emailInput, passwordInput]);

  const hasAnyErrors = () => emailErrorMessage.length > 0 || passwordErrorMessage.length > 0;

  const disableSubmit = () => {
    return emailInput.length * passwordInput.length == 0 || hasAnyErrors();
  };

  const _handleSubmit = async () => {
    const errorHandler = (error: AuthErrorModel) => {
      if (error.code.includes('password')) {
        setPasswordErrorMessage(t(error.code) || error.message);
      } else {
        setEmailErrorMessage(t(error.code) || error.message);
      }
    };

    setIsSubmitting(true);
    await props.handleSubmit(emailInput, passwordInput, errorHandler);
    setIsSubmitting(false);
  };

  return (
    <View style={styles.formContainer}>
      <View style={styles.formItemCol}>
        <Input
          leftIcon={<Icon name='email' size={24} color='white' />}
          testID='email'
          disabled={isSubmitting}
          value={emailInput}
          textContentType='emailAddress'
          keyboardType='email-address'
          inputStyle={styles.formInputText}
          onChangeText={setEmailInput}
          errorMessage={emailErrorMessage}
          renderErrorMessage={emailErrorMessage.length > 0}
          label={<Text style={styles.formLabel}>{t('input/label/email')}</Text>}
          placeholder={t('input/placeholder/email')}
        />
      </View>

      <View style={styles.formItemCol}>
        <Input
          leftIcon={<Icon name='lock' size={24} color='white' />}
          testID='pw'
          inputStyle={styles.formInputText}
          value={passwordInput}
          onSubmitEditing={_handleSubmit}
          disabled={emailInput.length == 0 || isSubmitting}
          onChangeText={setPasswordInput}
          placeholder={t('input/placeholder/password')}
          label={<Text style={styles.formLabel}>{t('input/label/password')}</Text>}
          secureTextEntry
          renderErrorMessage={passwordErrorMessage.length > 0}
          errorMessage={passwordErrorMessage}
          textContentType={'password'}
        />
      </View>

      <View style={styles.formItemRow}></View>
      <Button
        testID='submit'
        accessibilityLabel='submit'
        disabled={disableSubmit()}
        loading={isSubmitting}
        title={props.loginForm ? t('button/log-in') : t('button/new-user')}
        onPress={_handleSubmit}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  titleContainer: {
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: '#FFFFFF',
    fontSize: 32,
  },
  formContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 20,
  },
  formItemCol: {
    width: '80%',
    marginBottom: 20,
  },
  formLabel: {
    color: '#FFFFFF',
  },
  formInputText: {
    color: '#FFFFFF',
  },
  formInput: {
    borderRadius: 17.5,
    borderWidth: 1,
    borderColor: 'white',
    width: '100%',
    height: 35,
    padding: 10,
  },
  formItemRow: {
    width: '80%',
    flexDirection: 'row',
    marginBottom: 15,
  },
});

export default EmailPwForm;
