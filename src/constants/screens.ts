// All screen names
export enum Screens {
  //auth
  auth = 'auth', // the upper level name for auth stack
  authLoading = 'authloading',
  authentication = 'authentication',
  logIn = 'login',
  signIn = 'signin',

  app = 'app', // drawer wrapper for app stack
  home = 'home', // the upper level name for app stack
  dashboard = 'dashboard',
  trainingList = 'traininglist',
  session = 'session',
  profile = 'profile',
  profileEdit = 'profileedit',
  puttList = 'puttlist',
  puttDetails = 'puttdetails',
  sessionDetails = 'sessiondetails',
}
