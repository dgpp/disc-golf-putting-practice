export const themePalette = {
  backgroundPrimary: '#1b1e2b',
  backgroundSecondary: '#292d3e',
  border: '#11131b',
  textPrimary: 'white',
  textSecondary: '#c4c8e0',
  navIconDashboard: '#33ff88',
  percentageGood: '#33ff44',
  percentageAvg: '#aaee00',
  percentagePoor: '#ffaa00',
  percentageBad: '#ff0000',
};

export const getScoreColor = (percentage: number) => {
  if (percentage > 85) return themePalette.percentageGood;
  if (percentage > 65) return themePalette.percentageAvg;
  if (percentage > 35) return themePalette.percentagePoor;
  return themePalette.percentageBad;
};
