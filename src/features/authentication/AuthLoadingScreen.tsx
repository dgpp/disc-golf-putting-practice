import { t } from 'src/localization';
import React, { FunctionComponent } from 'react';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import { ScreenPropsModel } from 'src/models';
import { themePalette } from 'src/constants/theme';

interface IAuthLoadingScreenProps extends ScreenPropsModel {}

const AuthLoadingScreen: FunctionComponent<IAuthLoadingScreenProps> = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.loadingText}>{t('loading/logging-in')}</Text>
      <ActivityIndicator size={'large'} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: themePalette.backgroundSecondary,
  },
  loadingText: {
    fontSize: 24,
    color: 'white',
    marginBottom: 20,
  },
});

export default AuthLoadingScreen;
