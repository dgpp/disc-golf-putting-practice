import { t } from 'src/localization';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { Screens } from 'src/constants/screens';
import { ScreenPropsModel } from 'src/models';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from 'src/redux';
import AuthLoadingScreen from './AuthLoadingScreen';
import { LogInStatuses } from 'src/models/auth.model';
import { setLogInStatus, setUser } from './authSlice';
import { signInWithStoredUser } from 'src/api/auth';
import { themePalette } from 'src/constants/theme';

interface IAuthenticationScreenProps extends ScreenPropsModel {}

const AuthenticationScreen: FunctionComponent<IAuthenticationScreenProps> = (props) => {
  const dispatch = useAppDispatch();
  const { logInStatus } = useSelector((state: RootState) => state.auth);

  useEffect(() => {
    dispatch(setLogInStatus(LogInStatuses.Pending));
    const tryLoggingIn = async () => {
      try {
        const credential = await signInWithStoredUser();
        if (credential && credential.user) {
          dispatch(setUser(credential.user?.uid));
          dispatch(setLogInStatus(LogInStatuses.LoggedIn));
        } else {
          dispatch(setLogInStatus(LogInStatuses.LoggedOut));
        }
      } catch (error) {
        console.log(error);
        dispatch(setLogInStatus(LogInStatuses.LoggedOut));
      }
    };

    tryLoggingIn();
  }, []);
  const handleLoginWithEmail = () => {
    props.navigation.navigate(Screens.logIn);
  };

  const handleCreateNew = () => {
    props.navigation.navigate(Screens.signIn);
  };

  return logInStatus === LogInStatuses.Pending ? (
    <AuthLoadingScreen {...props} />
  ) : (
    <View style={styles.container}>
      <Button
        testID='login'
        onPress={handleLoginWithEmail}
        title={t('button/log-in-with-email')}
        titleStyle={{ marginLeft: 10 }}
        icon={<Icon name='email' size={24} color='white' />}
        containerStyle={styles.button}
      />
      <Button
        testID='create-user'
        title={t('button/create-new-user')}
        type='clear'
        onPress={handleCreateNew}
        containerStyle={styles.button}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: themePalette.backgroundSecondary,
  },
  button: {
    marginBottom: 20,
  },
});

export default AuthenticationScreen;
