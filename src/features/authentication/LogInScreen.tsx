import React from 'react';
import { View, StyleSheet } from 'react-native';
import { signInByMail } from 'src/api/auth';
import { AuthErrorModel, ScreenPropsModel } from 'src/models';
import { setUser, setLogInStatus } from './authSlice';
import { LogInStatuses } from 'src/models/auth.model';
import { useAppDispatch } from 'src/redux';
import EmailPwForm from 'src/components/emailpwform/EmailPwForm';
import { themePalette } from 'src/constants/theme';

interface ILogInScreenProps extends ScreenPropsModel {}

const LogInScreen = (props: ILogInScreenProps) => {
  const dispatch = useAppDispatch();

  const handleSubmit = async (email: string, pw: string, errorHandler: (error: AuthErrorModel) => any) => {
    const response = await signInByMail(email, pw, errorHandler);
    if (response) {
      const token = await response.user?.getIdToken();
      if (token && response.user?.uid) {
        dispatch(setUser(response.user?.uid));
        dispatch(setLogInStatus(LogInStatuses.LoggedIn));
      }
    }
  };

  return (
    <View style={styles.container}>
      <EmailPwForm handleSubmit={handleSubmit} loginForm />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    alignItems: 'center',
  },
});

export default LogInScreen;
