import React from 'react';
import { View, StyleSheet } from 'react-native';
import { createUserWithEmail } from 'src/api/auth';
import EmailPwForm from 'src/components/emailpwform/EmailPwForm';
import { themePalette } from 'src/constants/theme';
import { AuthErrorModel, ScreenPropsModel } from 'src/models';
import { LogInStatuses } from 'src/models/auth.model';
import { useAppDispatch } from 'src/redux';
import { setUser, setLogInStatus } from './authSlice';

interface ISignInScreenProps extends ScreenPropsModel {}

const SignInScreen = (prop: ISignInScreenProps) => {
  const dispatch = useAppDispatch();

  const handleSubmit = async (email: string, pw: string, errorHandler: (err: AuthErrorModel) => any) => {
    const response = await createUserWithEmail(email, pw, errorHandler);
    if (response) {
      const token = await response.user?.getIdToken();
      if (token && response.user?.uid) {
        dispatch(setUser(response.user?.uid));
        dispatch(setLogInStatus(LogInStatuses.LoggedIn));
      }
    }
  };

  return (
    <View style={styles.container}>
      <EmailPwForm handleSubmit={handleSubmit} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    alignItems: 'center',
  },
});

export default SignInScreen;
