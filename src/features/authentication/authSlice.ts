import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LogInStatuses } from 'src/models/auth.model';

export interface IAuthSlice {
  userId: string;
  logInStatus: LogInStatuses;
}

const initialState: IAuthSlice = {
  userId: '',
  logInStatus: LogInStatuses.Pending,
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<string>) {
      state.userId = action.payload;
    },
    setLogInStatus(state, action: PayloadAction<LogInStatuses>) {
      state.logInStatus = action.payload;
    },
  },
});

export const { setUser, setLogInStatus } = authSlice.actions;

export default authSlice.reducer;
