import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { themePalette, getScoreColor } from 'src/constants/theme';
import { ProgressCircle } from 'react-native-svg-charts';
import { calculateForStyle, calculateOverallPercentage } from './calcFunctions';
import { useSelector } from 'react-redux';
import { RootState } from 'src/redux';
import { PuttingStyles, PuttModel } from 'src/models';
import { Screens } from 'src/constants/screens';
import { getTranslatedPuttingStyle } from 'src/localization/dropDownValues';
import { t } from 'src/localization';

const DashboardScreen = (props: any) => {
  const { trainingList } = useSelector((state: RootState) => state.training);
  const [allPutts, setAllPutts] = useState<PuttModel[]>([]);

  useEffect(() => {
    getAllPutts();
  }, [trainingList]);

  const getAllPutts = () => {
    let putts: PuttModel[] = [];
    trainingList.forEach((tl) => {
      putts = [...putts, ...tl.putts];
    });

    setAllPutts(putts);
  };

  const handlePlay = () => {
    props.navigation.navigate(Screens.session);
  };

  const renderItem = (puttingStyle: PuttingStyles | null) => {
    const calculatedPercentage =
      puttingStyle !== null ? calculateForStyle(allPutts, puttingStyle) : calculateOverallPercentage(allPutts);

    const calculatedColor = getScoreColor(calculatedPercentage * 100);

    return (
      <View style={styles.dataRow}>
        <Text
          style={{
            ...styles.percentageText,
            color: themePalette.textPrimary,
          }}
        >
          {puttingStyle !== null ? getTranslatedPuttingStyle(puttingStyle) : t('dashboard/overall')}
        </Text>
        <View style={styles.progressChart}>
          <ProgressCircle
            animate
            style={{
              height: 100,
              width: 100,
            }}
            strokeWidth={10}
            progressColor={calculatedColor}
            progress={calculatedPercentage}
          />
          <Text
            style={{
              ...styles.percentageText,
              color: calculatedColor,
            }}
          >
            {Math.round(calculatedPercentage * 100)}%
          </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.graphContainer}>
        <FlatList
          data={[
            null, // for overall - check the renderItem function
            PuttingStyles.Staggered,
            PuttingStyles.Straddle,
            PuttingStyles.OnKnee,
            PuttingStyles.StepPutt,
            PuttingStyles.JumpPutt,
          ]}
          showsVerticalScrollIndicator
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => renderItem(item)}
        />
      </View>
      <View style={styles.playButtonContainer}>
        <TouchableOpacity style={styles.button} onPress={handlePlay}>
          <Text style={{ color: themePalette.textPrimary }}>{t('button/new-session')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

/**
 * Styles
 */

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: themePalette.backgroundPrimary,
  },
  graphContainer: {
    flex: 1,
    // backgroundColor: themePalette.backgroundPrimary,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 200,
    backgroundColor: themePalette.backgroundPrimary,
    marginTop: 10,
  },
  playButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    backgroundColor: themePalette.backgroundSecondary,
    borderTopWidth: 1,
    borderTopColor: themePalette.border,
    // backgroundColor: 'red',
    width: '100%',
  },
  dataRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 10,
    paddingTop: 10,
  },
  progressChart: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 170,
  },
  percentageText: {
    paddingHorizontal: 20,
    fontSize: 22,
  },
});

export default DashboardScreen;
