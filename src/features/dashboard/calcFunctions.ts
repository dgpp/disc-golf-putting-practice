import { PuttingStyles, PuttModel } from 'src/models';

/**
 *
 * @param putts provide all putts
 */
export const calculateOverallPercentage = (putts: PuttModel[]) => {
  const res =
    putts.length > 0
      ? Math.floor((putts.filter((p) => p.hit).length / putts.length) * 100) /
        100
      : 0;
  return res;
};

export const calculateForStyle = (putts: PuttModel[], style: PuttingStyles) => {
  return calculateOverallPercentage(
    putts.filter((p) => p.puttingStyle === style)
  );
};
