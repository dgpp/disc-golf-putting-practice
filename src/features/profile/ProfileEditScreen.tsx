import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableNativeFeedback, Alert } from 'react-native';
import { Icon, Input, Text } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { updateDisplayName } from 'src/api/profile';
import { themePalette } from 'src/constants/theme';
import { t } from 'src/localization';
import { ScreenPropsModel } from 'src/models';
import { RootState, useAppDispatch } from 'src/redux';
import { setDisplayName } from './profileSlice';

interface ProfileEditScreen extends ScreenPropsModel {}

const ProfileEditScreen = (props: ProfileEditScreen) => {
  const {
    profile: { displayName },
    auth: { userId },
  } = useSelector((state: RootState) => state);
  const [newDisplayName, setNewDisplayName] = useState(displayName);
  const [dirty, setDirty] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  useEffect(() => {
    setNewDisplayName(displayName);
  }, []);

  const isValid = () => dirty && newDisplayName.length > 0;

  useEffect(
    () =>
      props.navigation.addListener('beforeRemove', (e) => {
        if (!isValid() || !saved) return;

        e.preventDefault();

        Alert.alert(
          t('edit-profile/discard-dialog/title'),
          t('edit-profile/discard-dialog/body'),
          [
            { text: t('stay'), style: 'cancel', onPress: () => {} },
            {
              text: t('leave'),
              style: 'destructive',

              onPress: () => props.navigation.dispatch(e.data.action),
            },
          ]
        );
      }),
    [props.navigation, dirty]
  );

  const handleTextChange = (text: string) => {
    setDirty(text !== displayName);
    setNewDisplayName(text);
  };

  const handleSaveButton = async () => {
    const res = updateDisplayName(userId, newDisplayName);
    if (!res) return;

    dispatch(setDisplayName(newDisplayName));
    setSaved(true);
    props.navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <View style={styles.upperContainer}>
        <Text h4 style={{ ...styles.textPrimary, marginTop: 20 }}>
          {t('edit-profile/edit-here')}
        </Text>
        <View style={styles.contentContainer}></View>
        <View style={styles.contentContainer}>
          <Text style={styles.textPrimary}>{t('display-name')}</Text>

          <Input
            value={newDisplayName}
            onChangeText={handleTextChange}
            containerStyle={styles.inputContainer}
            style={{ color: themePalette.textPrimary }}
            maxLength={20}
          />
          <Text style={styles.characterCount}>
            {newDisplayName.length + '/20'}
          </Text>
        </View>
      </View>
      <View style={styles.footerContainer}>
        <TouchableNativeFeedback disabled={!dirty} onPress={handleSaveButton}>
          <View style={styles.footerButtonContainer}>
            <Icon
              disabled={!isValid()}
              disabledStyle={{
                backgroundColor: themePalette.backgroundPrimary,
                opacity: 0.3,
              }}
              name={'save'}
              size={32}
              color={themePalette.textPrimary}
            />
            <Text
              style={{
                color: themePalette.textPrimary,
                opacity: isValid() ? 1 : 0.3,
              }}
            >
              {t('edit-profile/save-profile')}
            </Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    </View>
  );
};

export default ProfileEditScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    alignItems: 'center',
  },
  upperContainer: {
    flex: 1,
  },
  contentContainer: {
    padding: 20,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  characterCount: {
    color: themePalette.textSecondary,
    fontSize: 14,
    marginTop: -15,
    alignSelf: 'flex-end',
  },
  footerContainer: {
    justifyContent: 'flex-end',
    backgroundColor: themePalette.backgroundPrimary,
    borderTopWidth: 1,
    borderTopColor: themePalette.border,
    alignItems: 'flex-end',
    padding: 20,
    width: '100%',
  },
  textPrimary: {
    color: themePalette.textPrimary,
    fontSize: 24,
  },
  footerButtonContainer: {
    flexDirection: 'column',
  },
  inputContainer: {
    width: 200,
    padding: 0,
    margin: 0,
  },
});
