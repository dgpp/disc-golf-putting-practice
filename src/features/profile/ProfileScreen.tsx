import React, { FunctionComponent } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Text } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { signOut } from 'src/api/auth';
import { Screens } from 'src/constants/screens';
import { themePalette } from 'src/constants/theme';
import { t } from 'src/localization';
import { ScreenPropsModel } from 'src/models';
import { LogInStatuses } from 'src/models/auth.model';
import { RootState, useAppDispatch } from 'src/redux';
import { setLogInStatus, setUser } from '../authentication/authSlice';
import { setTrainingList } from '../training/trainingSlice';

interface IProfileScreenProps extends ScreenPropsModel {}

const ProfileScreen: FunctionComponent<IProfileScreenProps> = (props) => {
  const {
    profile: { displayName },
  } = useSelector((state: RootState) => state);
  const dispatch = useAppDispatch();

  const handleSignOut = () => {
    signOut().then(() => {
      dispatch(setUser(''));
      dispatch(setLogInStatus(LogInStatuses.LoggedOut));
    });
  };

  const handleNavProfileEdit = () => {
    props.navigation.navigate(Screens.profileEdit);
  };
  return (
    <View style={styles.container}>
      <Text
        h1
        style={{ color: themePalette.textSecondary, textAlign: 'center' }}
      >
        {t('profile/hello') + ', '}
        <Text h1 style={{ color: themePalette.percentagePoor }}>
          {displayName}
        </Text>
        <Text h1 style={{ color: themePalette.textSecondary }}>
          {'!'}
        </Text>
      </Text>
      <View>
        <TouchableOpacity style={styles.button} onPress={handleNavProfileEdit}>
          <Text style={{ color: themePalette.textPrimary }}>
            {t('profile/edit-profile')}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={handleSignOut}>
          <Text style={{ color: themePalette.textPrimary }}>
            {t('profile/sign-out')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: themePalette.backgroundSecondary,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 200,
    backgroundColor: themePalette.backgroundPrimary,
    marginTop: 10,
  },
});

export default ProfileScreen;
