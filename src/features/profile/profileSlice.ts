import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IProfileSlice {
  displayName: string;
  email: string;
}

const initialState: IProfileSlice = {
  displayName: '',
  email: '',
};

const profileSlice = createSlice({
  name: 'training',
  initialState,
  reducers: {
    setDisplayName(state, action: PayloadAction<string>) {
      state.displayName = action.payload;
    },
    setEmail(state, action: PayloadAction<string>) {
      state.email = action.payload;
    },
  },
});

export const { setDisplayName, setEmail } = profileSlice.actions;

export default profileSlice.reducer;
