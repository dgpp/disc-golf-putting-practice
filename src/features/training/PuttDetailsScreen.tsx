import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { themePalette } from 'src/constants/theme';
import { t } from 'src/localization';
import {
  getTranslatedPuttingStyle,
  getTranslatedWindDirection,
} from 'src/localization/dropDownValues';
import { ScreenPropsModel } from 'src/models';
import { RootState, useAppDispatch } from 'src/redux';
import { setSelectedPutt } from './trainingSlice';

interface PuttDetailsScreenProps extends ScreenPropsModel {}

const PuttDetailsScreen = (props: PuttDetailsScreenProps) => {
  const { selectedPutt } = useSelector((state: RootState) => state.training);
  const dispatch = useAppDispatch();

  useEffect(
    () =>
      props.navigation.addListener('beforeRemove', (e) => {
        dispatch(setSelectedPutt(null));
      }),
    [props.navigation]
  );

  return (
    selectedPutt && (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text h2 style={styles.textPrimary}>
            {'Putt #' + (selectedPutt.id + 1)}
          </Text>
        </View>
        <View style={styles.row}>
          <View style={styles.colLeft}>
            <Text style={styles.textPrimary}>{t('putting-style')}</Text>
          </View>
          <View style={styles.colRight}>
            <Text style={styles.textSecondary}>
              {getTranslatedPuttingStyle(selectedPutt.puttingStyle)}
            </Text>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.colLeft}>
            <Text style={styles.textPrimary}>{t('distance')}</Text>
          </View>
          <View style={styles.colRight}>
            <Text style={styles.textSecondary}>
              {selectedPutt.range + ' ft'}
            </Text>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.colLeft}>
            <Text style={styles.textPrimary}>{t('wind-strength')}</Text>
          </View>
          <View style={styles.colRight}>
            <Text style={styles.textSecondary}>
              {selectedPutt.wind + ' m/s'}
            </Text>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.colLeft}>
            <Text style={styles.textPrimary}>{t('wind-direction')}</Text>
          </View>
          <View style={styles.colRight}>
            <Text style={styles.textSecondary}>
              {getTranslatedWindDirection(selectedPutt.windDirection)}
            </Text>
          </View>
        </View>
        <View style={styles.bottomContainer}>
          <Text
            h1
            style={{
              color: selectedPutt.hit
                ? themePalette.percentageGood
                : themePalette.percentageBad,
            }}
          >
            {selectedPutt.hit ? t('hit') : t('miss')}
          </Text>
        </View>
      </View>
    )
  );
};

export default PuttDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
  },
  bottomContainer: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContainer: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    width: '100%',
    padding: 20,
  },
  row: {
    flexDirection: 'row',
  },
  colLeft: {
    width: '50%',
    alignItems: 'flex-end',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  colRight: {
    width: '50%',
    alignItems: 'flex-start',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  textPrimary: {
    color: themePalette.textPrimary,
    fontSize: 16,
  },
  textSecondary: {
    color: themePalette.textSecondary,
    fontSize: 16,
  },
});
