import React, { useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TouchableNativeFeedback,
} from 'react-native';
import { Icon, ListItem } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { Screens } from 'src/constants/screens';
import { themePalette } from 'src/constants/theme';
import { getTranslatedPuttingStyle } from 'src/localization/dropDownValues';
import { PuttingStyles, PuttModel, ScreenPropsModel } from 'src/models';
import { RootState, useAppDispatch } from 'src/redux';
import { setSelectedPutt } from './trainingSlice';

interface PuttListScreenProps extends ScreenPropsModel {}

const PuttListScreen = (props: PuttListScreenProps) => {
  const { selectedSession } = useSelector((state: RootState) => state.training);
  const dispatch = useAppDispatch();

  const openPuttDetails = (putt: PuttModel) => {
    dispatch(setSelectedPutt(putt));
    props.navigation.navigate(Screens.puttDetails);
  };

  const renderItem = (putt: PuttModel) => (
    <ListItem
      containerStyle={styles.itemContainer}
      Component={TouchableNativeFeedback}
      bottomDivider
      onPress={() => {
        openPuttDetails(putt);
      }}
    >
      <ListItem.Content>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
          }}
        >
          <ListItem.Title style={{ color: themePalette.textPrimary }}>
            {'#' +
              (putt.id + 1) +
              ' - ' +
              getTranslatedPuttingStyle(putt.puttingStyle)}
          </ListItem.Title>

          <Icon
            name={putt.hit ? 'done' : 'clear'}
            size={26}
            color={
              putt.hit
                ? themePalette.percentageGood
                : themePalette.percentageBad
            }
          />
        </View>
      </ListItem.Content>
      <ListItem.Chevron size={30} />
    </ListItem>
  );

  return (
    selectedSession && (
      <View style={styles.container}>
        <FlatList
          data={selectedSession.putts}
          renderItem={(p) => renderItem(p.item)}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    )
  );
};

export default PuttListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
  },
  itemContainer: {
    backgroundColor: themePalette.backgroundPrimary,
  },
  textPrimary: {
    color: themePalette.textPrimary,
  },
});
