import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableNativeFeedback } from 'react-native';
import { Icon, Text } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { Screens } from 'src/constants/screens';
import { themePalette } from 'src/constants/theme';
import { t } from 'src/localization';
import { getTranslatedPuttingStyle } from 'src/localization/dropDownValues';
import { PuttingStyles, PuttModel, ScreenPropsModel } from 'src/models';
import { RootState, useAppDispatch } from 'src/redux';
import { setSelectedSession } from './trainingSlice';

interface SessionDetailsScreenProps extends ScreenPropsModel {}

const SessionDetailsScreen = (props: SessionDetailsScreenProps) => {
  const { selectedSession } = useSelector((state: RootState) => state.training);
  const [puttsByStyles, setPuttsByStyles] = useState<{
    [name: string]: PuttModel[];
  }>({});

  useEffect(
    () =>
      setPuttsByStyles(
        selectedSession
          ? {
              [PuttingStyles.Straddle]: selectedSession.putts.filter(
                (p) => p.puttingStyle === PuttingStyles.Straddle
              ),
              [PuttingStyles.Staggered]: selectedSession.putts.filter(
                (p) => p.puttingStyle === PuttingStyles.Staggered
              ),
              [PuttingStyles.OnKnee]: selectedSession.putts.filter(
                (p) => p.puttingStyle === PuttingStyles.OnKnee
              ),
              [PuttingStyles.JumpPutt]: selectedSession.putts.filter(
                (p) => p.puttingStyle === PuttingStyles.JumpPutt
              ),
              [PuttingStyles.StepPutt]: selectedSession.putts.filter(
                (p) => p.puttingStyle === PuttingStyles.StepPutt
              ),
            }
          : {}
      ),
    [selectedSession]
  );

  const dispatch = useAppDispatch();
  const allStyles = [
    PuttingStyles.Staggered,
    PuttingStyles.Straddle,
    PuttingStyles.OnKnee,
    PuttingStyles.StepPutt,
    PuttingStyles.JumpPutt,
  ];

  useEffect(
    () =>
      props.navigation.addListener('beforeRemove', (e) => {
        dispatch(setSelectedSession(null));
      }),
    [props.navigation]
  );

  const getFormatedDuration = () => {
    if (!selectedSession) return "can't retrieve duration";

    const { hours, minutes, seconds } = selectedSession.duration;
    const h = hours > 0 ? hours + ` ${t('time/hours')}, \n` : '';
    const m = minutes > 0 ? minutes + ` ${t('time/minutes')}, \n` : '';
    const s = seconds > 0 ? seconds + ` ${t('time/seconds')}, \n` : '';

    return h + m + s;
  };

  const openPuttList = () => {
    props.navigation.navigate(Screens.puttList);
  };

  const calculateWindSpeed = () => {
    if (!selectedSession) return 0;

    const avg =
      selectedSession.putts.reduce((a, c) => a + c.wind, 0) /
      selectedSession.putts.length;

    return Math.floor(avg * 100) / 100;
  };

  const getPuttsByStyle = (puttingStyle: PuttingStyles, index: number) =>
    puttsByStyles[puttingStyle] &&
    puttsByStyles[puttingStyle].length > 0 && (
      <View key={index.toString()} style={styles.contentRow}>
        <View style={styles.colLeft}>
          <Text style={styles.secondaryText}>
            {getTranslatedPuttingStyle(puttingStyle)}
          </Text>
        </View>
        <View style={styles.colRight}>
          <Text style={styles.secondaryText}>
            {puttsByStyles[puttingStyle].filter((p) => p.hit).length +
              ` ${t('of')} ` +
              puttsByStyles[puttingStyle].length}
          </Text>
        </View>
      </View>
    );

  return selectedSession ? (
    <>
      <ScrollView style={styles.scrollableContainer}>
        <View style={styles.container}>
          <View style={styles.contentHeader}>
            <Text h2 style={styles.primaryText}>
              {new Date(selectedSession.date).toDateString()}
            </Text>
          </View>

          <View style={styles.contentHeader}>
            <Text h4 style={styles.primaryText}>
              {t('session-duration')}
            </Text>
          </View>

          <View style={styles.contentRow}>
            <Text style={styles.secondaryText}>{getFormatedDuration()}</Text>
          </View>

          <View style={styles.contentHeader}>
            <Text h4 style={styles.primaryText}>
              {t('session-details/putts-by-styles')}
            </Text>
          </View>

          {allStyles.map((s, index) => getPuttsByStyle(s, index))}

          <View style={styles.contentHeader}>
            <Text
              h4
              style={
                calculateWindSpeed() > 0
                  ? styles.primaryText
                  : styles.secondaryText
              }
            >
              {calculateWindSpeed() > 0
                ? t('session-details/avg-wind')
                : t('session-details/wind-calm')}
            </Text>
          </View>
          <View style={styles.contentRow}>
            {calculateWindSpeed() > 0 && (
              <Text style={styles.secondaryText}>
                {calculateWindSpeed() + ' m/s'}
              </Text>
            )}
          </View>
        </View>
      </ScrollView>
      <View style={styles.footerContainer}>
        <TouchableNativeFeedback onPress={openPuttList}>
          <View style={styles.contentRow}>
            <Text style={styles.primaryText}>
              {t('session-details/go-to-puttlist')}
            </Text>
            <Icon name={'navigate-next'} color='white' size={32} />
          </View>
        </TouchableNativeFeedback>
      </View>
    </>
  ) : (
    <View style={styles.container}>
      <Text style={styles.primaryText}>{t('something-went-wrong')}</Text>
    </View>
  );
};

export default SessionDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
  },
  scrollableContainer: {
    backgroundColor: themePalette.backgroundSecondary,
  },
  contentRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentHeader: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  colLeft: {
    width: '50%',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  colRight: {
    width: '50%',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  footerContainer: {
    alignItems: 'flex-end',
    padding: 20,
    backgroundColor: themePalette.backgroundPrimary,
  },
  primaryText: {
    color: themePalette.textPrimary,
    fontSize: 20,
  },
  secondaryText: {
    color: themePalette.textSecondary,
    fontSize: 16,
  },
});
