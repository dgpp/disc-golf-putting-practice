import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, FlatList, TouchableNativeFeedback, Alert } from 'react-native';
import { Icon, Input } from 'react-native-elements';
import { useSelector } from 'react-redux';
import { saveSessions } from 'src/api/database';
import { themePalette } from 'src/constants/theme';
import {
  PuttingStyles,
  PuttModel,
  ScreenPropsModel,
  TrainingDurationModel,
  TrainingSessionModel,
  WindDirections,
} from 'src/models';
import { RootState, useAppDispatch } from 'src/redux';
import { setTrainingList } from './trainingSlice';
import { getTranslatedPuttingStyle, getTranslatedWindDirection } from 'src/localization/dropDownValues';
import { t } from 'src/localization';
import { Picker } from '@react-native-community/picker';

const PuttItem = ({
  item,
  removeHandler,
  updateHandler,
}: {
  item: PuttModel;
  removeHandler: any;
  updateHandler: any;
}) => {
  const [ownItem, setOwnItem] = useState({ ...item });

  useEffect(() => {
    updateHandler(ownItem);
  }, [ownItem]);

  return (
    <View key={item.id} style={styles.itemContainer}>
      <Text style={styles.textPrimary}>#{item.id + 1}</Text>
      <View style={styles.rangeContainer}>
        <Text style={styles.textPrimary}>
          {item.range}
          {' ft'}
        </Text>
      </View>
      <View style={styles.rangeContainer}>
        <Text style={styles.textPrimary}>{getTranslatedPuttingStyle(item.puttingStyle)}</Text>
      </View>
      <View style={styles.rangeContainer}>
        <Icon
          name={item.hit ? 'done' : 'clear'}
          color={item.hit ? themePalette.percentageGood : themePalette.percentageBad}
          size={24}
          style={{ paddingLeft: 20 }}
        />
      </View>

      <Icon
        name='delete'
        size={32}
        color={themePalette.textSecondary}
        onPress={() => {
          removeHandler(item.id);
        }}
      />
    </View>
  );
};
interface TrainingProps extends ScreenPropsModel {}

const Training = (props: TrainingProps) => {
  const dispatch = useAppDispatch();

  const [putts, setPutts] = useState<PuttModel[]>([]);
  const [saved, setSaved] = useState<boolean>(false);
  const [editSettings, setEditSettings] = useState<boolean>(true);
  const [defaultDistance, setDefaultDistance] = useState<number>(20);
  const [defaultPuttingStyle, setDefaultPuttingStyle] = useState<PuttingStyles>(PuttingStyles.Staggered);
  const [defaultWindSpeed, setDefaultWindSpeed] = useState<number>(0);
  const [defaultWindDirection, setDefaultWindDirection] = useState<WindDirections>(WindDirections.HeadWind);

  const [startTime, setStartTime] = useState<number>(0);

  const {
    training: { trainingList },
    auth: { userId },
  } = useSelector((state: RootState) => state);

  useEffect(() => {
    setStartTime(Date.now());
  }, []);

  useEffect(
    () =>
      props.navigation.addListener('beforeRemove', (e) => {
        if (putts.length === 0 || saved) return;

        e.preventDefault();

        Alert.alert(t('session/leave-without-save/title'), t('session/leave-without-save/body'), [
          { text: t('stay'), style: 'cancel', onPress: () => {} },
          {
            text: t('leave'),
            style: 'destructive',

            onPress: () => props.navigation.dispatch(e.data.action),
          },
        ]);
      }),
    [props.navigation, putts, saved]
  );

  const sortPuttsById = (puttsArg: PuttModel[]) => {
    const sortedPutts = [...puttsArg]
      .sort((a, b) => a.id - b.id)
      .map((p, i) => {
        return { ...p, id: i };
      })
      .reverse();

    return [...sortedPutts];
  };

  const calculateDuration = (): TrainingDurationModel => {
    let secondsLeft = (Date.now() - startTime) / 1000;

    const hours = Math.floor(secondsLeft / 60 / 60);
    secondsLeft -= hours * 60 * 60;

    const minutes = Math.floor(secondsLeft / 60);
    secondsLeft -= minutes * 60;

    const duration: TrainingDurationModel = {
      hours,
      minutes,
      seconds: Math.floor(secondsLeft),
    };

    return duration;
  };

  const saveSession = () => {
    Alert.alert(
      t('session/save-dialog/title'),
      t('session/save-dialog/body'),
      [
        {
          text: t('cancel'),
          style: 'cancel',
        },
        {
          text: t('save'),
          onPress: () => {
            save();
          },
        },
      ],
      { cancelable: true }
    );

    const save = () => {
      const thisSession: TrainingSessionModel = {
        id: trainingList.length,
        date: Date.now(),
        putts: [...putts].reverse(),
        duration: calculateDuration(),
      };

      setSaved(true);
      const updatedSessions = [...trainingList, thisSession];
      dispatch(setTrainingList(updatedSessions));
      saveSessions(userId, updatedSessions);
      props.navigation.goBack();
    };
  };

  const addNewPutt = (hit: boolean) => {
    const newPutts = sortPuttsById(putts);
    const initPutt: PuttModel = {
      id: putts.length,
      puttingStyle: defaultPuttingStyle,
      hit: hit,
      range: defaultDistance,
      wind: defaultWindSpeed,
      windDirection: defaultWindDirection,
    };

    setPutts([initPutt, ...newPutts]);
  };

  const removePutt = (id: number) => {
    const puttFiltered = putts.filter((p) => p.id !== id);
    const sortedPutts = sortPuttsById(puttFiltered);

    setPutts([...sortedPutts]);
  };

  const updatePutt = (putt: PuttModel) => {
    const index = putts.findIndex((p) => p.id === putt.id);
    const newPutts = [...putts];
    newPutts[index] = putt;
    setPutts(newPutts);
  };

  const toggleSettings = () => setEditSettings(!editSettings);

  const updateDefaultDistance = (val: string) => {
    const parsed = parseInt(val);
    setDefaultDistance(!isNaN(parsed) ? parsed : 0);
  };

  const updateDefaultWindSpeed = (val: string) => {
    const parsed = parseInt(val);
    setDefaultWindSpeed(!isNaN(parsed) ? parsed : 0);
  };

  return (
    (editSettings && (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <View style={styles.settingsRow}>
            <Text style={styles.textPrimary}>{t('putting-style')}</Text>
            <View style={styles.pickerStyle}>
              <Picker
                selectedValue={defaultPuttingStyle}
                style={{ color: 'white', width: '100%' }}
                onValueChange={(value, idx: number) => {
                  const val: PuttingStyles = value as PuttingStyles;
                  setDefaultPuttingStyle(val);
                }}
              >
                <Picker.Item
                  label={getTranslatedPuttingStyle(PuttingStyles.Staggered)}
                  value={PuttingStyles.Staggered}
                />
                <Picker.Item label={getTranslatedPuttingStyle(PuttingStyles.Straddle)} value={PuttingStyles.Straddle} />
                <Picker.Item label={getTranslatedPuttingStyle(PuttingStyles.OnKnee)} value={PuttingStyles.OnKnee} />
                <Picker.Item label={getTranslatedPuttingStyle(PuttingStyles.StepPutt)} value={PuttingStyles.StepPutt} />
                <Picker.Item label={getTranslatedPuttingStyle(PuttingStyles.JumpPutt)} value={PuttingStyles.JumpPutt} />
              </Picker>
            </View>
          </View>
          <View style={styles.settingsRow}>
            <Text style={styles.textPrimary}>{t('distance')}</Text>
            <Input
              containerStyle={styles.settingsInputContainer}
              keyboardType={'numeric'}
              style={{ color: 'white' }}
              maxLength={3}
              value={defaultDistance.toString()}
              onChangeText={(val) => {
                updateDefaultDistance(val);
              }}
            />
            <Text style={styles.textPrimary}>{' ft'}</Text>
          </View>
          <View style={styles.settingsRow}>
            <Text style={styles.textPrimary}>{t('wind-strength')}</Text>
            <Input
              containerStyle={styles.settingsInputContainer}
              style={{ color: 'white' }}
              keyboardType={'numeric'}
              maxLength={3}
              value={defaultWindSpeed.toString()}
              onChangeText={(val) => {
                updateDefaultWindSpeed(val);
              }}
            />
            <Text style={styles.textPrimary}>{' m/s'}</Text>
          </View>
          <View style={styles.settingsRow}>
            <Text style={styles.textPrimary}>{t('wind-direction')}</Text>
            <View style={styles.pickerStyle}>
              <Picker
                selectedValue={defaultWindDirection}
                style={{ color: 'white', width: '100%' }}
                onValueChange={(value, idx: number) => {
                  const val: WindDirections = value as WindDirections;
                  setDefaultWindDirection(val);
                }}
              >
                <Picker.Item
                  label={getTranslatedWindDirection(WindDirections.HeadWind)}
                  value={WindDirections.HeadWind}
                />
                <Picker.Item
                  label={getTranslatedWindDirection(WindDirections.TailWind)}
                  value={WindDirections.TailWind}
                />
                <Picker.Item
                  label={getTranslatedWindDirection(WindDirections.CrossLeft)}
                  value={WindDirections.CrossLeft}
                />
                <Picker.Item
                  label={getTranslatedWindDirection(WindDirections.CrossRight)}
                  value={WindDirections.CrossRight}
                />
              </Picker>
            </View>
          </View>
        </View>
        <View style={styles.footerContainer}>
          <TouchableNativeFeedback onPress={toggleSettings}>
            <Text style={styles.textPrimary}>{t('ok')}</Text>
          </TouchableNativeFeedback>
        </View>
      </View>
    )) || (
      <View style={styles.container}>
        <FlatList
          data={putts}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => <PuttItem item={item} removeHandler={removePutt} updateHandler={updatePutt} />}
        />
        <View style={styles.addItemRow}>
          <View style={{ alignItems: 'center' }}>
            <Text style={{ color: themePalette.percentageBad }}>{t('miss')}</Text>
            <Icon
              name='add-circle-outline'
              color={themePalette.percentageBad}
              size={60}
              onPress={() => {
                addNewPutt(false);
              }}
              style={{ paddingLeft: 20 }}
            />
          </View>
          <View style={{ alignItems: 'center' }}>
            <Text style={{ color: themePalette.percentageGood }}>{t('hit')}</Text>
            <Icon
              name='add-circle-outline'
              color={themePalette.percentageGood}
              size={60}
              onPress={() => {
                addNewPutt(true);
              }}
              style={{ paddingLeft: 20 }}
            />
          </View>
        </View>
        <View style={styles.itemContainer}>
          <View style={{ alignItems: 'center' }}>
            <Icon name='settings' color='white' size={34} onPress={toggleSettings} style={{ paddingLeft: 20 }} />
            <Text style={{ color: themePalette.textPrimary }}>{t('session/edit-settings')}</Text>
          </View>
          <View>
            <View style={{ alignItems: 'center' }}>
              <Icon
                disabled={putts.length === 0}
                disabledStyle={{
                  backgroundColor: themePalette.backgroundPrimary,
                  opacity: 0.3,
                }}
                name='save'
                color='white'
                size={34}
                onPress={saveSession}
                style={{ paddingLeft: 20 }}
              />
              <Text
                style={{
                  color: themePalette.textPrimary,
                  opacity: putts.length === 0 ? 0.3 : 1,
                }}
              >
                {t('session/save-session')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  );
};

export default Training;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
  },
  contentContainer: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    padding: 20,
    alignItems: 'center',
  },
  settingsRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '85%',
    marginTop: '10%',
  },
  settingsInputContainer: {
    maxWidth: 100,
  },
  textPrimary: {
    color: themePalette.textPrimary,
    fontSize: 18,
  },
  pickerStyle: {
    color: 'white',
    borderBottomWidth: 1,
    borderColor: '#707080',
    width: '50%',
  },
  addItemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 20,
    backgroundColor: themePalette.backgroundPrimary,
    borderTopWidth: 1,
    borderColor: themePalette.border,
  },
  itemContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: themePalette.backgroundPrimary,
    borderColor: themePalette.border,
    borderTopWidth: 1,
    minHeight: 50,
    padding: 20,
  },
  rangeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  inputContainer: {
    width: 55,
    padding: 0,
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    margin: 0,
  },
  checkBoxContainer: {
    backgroundColor: themePalette.backgroundSecondary,
    borderColor: themePalette.border,
  },
  footerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: 20,
    borderTopColor: themePalette.border,
    borderTopWidth: 1,
    backgroundColor: themePalette.backgroundPrimary,
  },
});
