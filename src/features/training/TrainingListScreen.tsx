import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  FlatList,
  ListRenderItemInfo,
  View,
  TouchableNativeFeedback,
  StyleSheet,
  TouchableOpacity,
  I18nManager,
} from 'react-native';
import { PuttModel, ScreenPropsModel, TrainingSessionModel } from 'src/models';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from 'src/redux';
import { ListItem, Text } from 'react-native-elements';
import { getScoreColor, themePalette } from 'src/constants/theme';
import { setSelectedSession } from './trainingSlice';
import { Screens } from 'src/constants/screens';
import { t } from 'src/localization';
import * as Localization from 'expo-localization';

interface ITrainingListScreenProps extends ScreenPropsModel {}

const TrainingListScreen: FunctionComponent<ITrainingListScreenProps> = (props) => {
  const { trainingList } = useSelector((state: RootState) => state.training);
  const dispatch = useAppDispatch();

  const calculatePercentage = (putts: PuttModel[]) =>
    Math.round((putts.filter((p) => p.hit).length / putts.length) * 100);

  const handlePlay = () => {
    props.navigation.navigate(Screens.session);
  };

  const renderItem = ({ item }: ListRenderItemInfo<TrainingSessionModel>) => {
    return (
      <ListItem
        containerStyle={styles.listItemContainer}
        Component={TouchableNativeFeedback}
        bottomDivider
        onPress={() => {
          dispatch(setSelectedSession(item));
          props.navigation.navigate(Screens.sessionDetails);
        }}
      >
        <ListItem.Content>
          <ListItem.Title style={{ color: themePalette.textPrimary }}>
            {new Date(item.date).toLocaleDateString(Localization.locale)}
          </ListItem.Title>

          <ListItem.Subtitle style={{ color: themePalette.textSecondary }}>
            {t('session-duration') +
              ': ' +
              (item.duration.hours ? item.duration.hours + 'h ' : '') +
              (item.duration.minutes ? item.duration.minutes + 'm ' : '') +
              (item.duration.seconds ? item.duration.seconds + 's ' : '')}
          </ListItem.Subtitle>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
            <Text style={{ color: themePalette.textPrimary }}>{t('putts') + ': '}</Text>
            <Text style={{ color: getScoreColor(calculatePercentage(item.putts)) }}>
              {calculatePercentage(item.putts) + '% '}
            </Text>
            <Text style={{ color: themePalette.textSecondary }}>
              {' (' + item.putts.filter((p) => p.hit).length + ` ${t('of')} ` + item.putts.length + ')'}
            </Text>
          </View>
        </ListItem.Content>
        <ListItem.Chevron size={30} />
      </ListItem>
    );
  };

  return (
    (trainingList.length > 0 && (
      <View style={styles.container}>
        <View style={styles.listContainer}>
          <FlatList data={trainingList} keyExtractor={(item) => item.id.toString()} renderItem={renderItem} />
        </View>
        <View style={styles.playButtonContainer}>
          <TouchableOpacity style={styles.button} onPress={handlePlay}>
            <Text style={{ color: themePalette.textPrimary }}>{t('button/new-session')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )) || (
      <View style={styles.lowerContainer}>
        <Text h4 style={styles.textSecondary}>
          {t('training-list/no-sessions')}
        </Text>

        <TouchableOpacity style={styles.button} onPress={handlePlay}>
          <Text style={{ color: themePalette.textPrimary }}>{t('button/new-session')}</Text>
        </TouchableOpacity>
      </View>
    )
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: themePalette.backgroundPrimary,
  },
  listContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: themePalette.backgroundPrimary,
  },
  listItemContainer: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: themePalette.backgroundPrimary,
  },
  lowerContainer: {
    flex: 1,
    backgroundColor: themePalette.backgroundSecondary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSecondary: {
    color: themePalette.textSecondary,
    fontSize: 16,
  },
  textPrimary: {
    color: themePalette.textPrimary,
    fontSize: 20,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 200,
    backgroundColor: themePalette.backgroundPrimary,
    marginTop: 10,
  },
  playButtonContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    backgroundColor: themePalette.backgroundSecondary,
    borderTopWidth: 1,
    borderTopColor: themePalette.border,
    width: '100%',
  },
});

export default TrainingListScreen;
