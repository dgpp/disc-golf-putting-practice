import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  TrainingSessionModel,
  PuttingStyles,
  TrainingDurationModel,
  PuttModel,
} from 'src/models';

export interface ITrainingSlice {
  trainingList: TrainingSessionModel[];
  selectedSession: TrainingSessionModel | null;
  selectedPutt: PuttModel | null;
}

const initialState: ITrainingSlice = {
  trainingList: [],
  selectedSession: null,
  selectedPutt: null,
};

const trainingSlice = createSlice({
  name: 'training',
  initialState,
  reducers: {
    setTrainingList(state, action: PayloadAction<TrainingSessionModel[]>) {
      state.trainingList = action.payload;
    },
    setSelectedSession(
      state,
      action: PayloadAction<TrainingSessionModel | null>
    ) {
      state.selectedSession = action.payload;
    },
    setSelectedPutt(state, action: PayloadAction<PuttModel | null>) {
      state.selectedPutt = action.payload;
    },
  },
});

export const {
  setTrainingList,
  setSelectedSession,
  setSelectedPutt,
} = trainingSlice.actions;

export default trainingSlice.reducer;
