import { PuttingStyles, WindDirections } from 'src/models';
import { t } from '.';

export const getTranslatedPuttingStyle = (puttingStyle: PuttingStyles) => {
  switch (puttingStyle) {
    case PuttingStyles.Straddle:
      return t('putting-style/straddle');
    case PuttingStyles.Staggered:
      return t('putting-style/staggered');
    case PuttingStyles.OnKnee:
      return t('putting-style/on-knee');
    case PuttingStyles.JumpPutt:
      return t('putting-style/jump-putt');
    case PuttingStyles.StepPutt:
      return t('putting-style/step-putt');
    default:
      return 'err';
  }
};

export const getTranslatedWindDirection = (windDirection: WindDirections) => {
  switch (windDirection) {
    case WindDirections.CrossLeft:
      return t('wind-direction/right-to-left');
    case WindDirections.CrossRight:
      return t('wind-direction/left-to-right');
    case WindDirections.HeadWind:
      return t('wind-direction/headwind');
    case WindDirections.TailWind:
      return t('wind-direction/tailwind');
    default:
      return 'err';
  }
};
