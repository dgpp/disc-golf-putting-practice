import memoize from 'lodash/memoize';
import i18n from 'i18n-js';
import * as Localization from 'expo-localization';
import fi from './fi.json';
import en from './en.json';
import { AuthErrorCodes } from 'src/models';

// later on we may want to import only language the device is using,
// two languages will be fine to be importand allways

// export const translationGetters = {
//   'fi': () => require('./fi.json'),
//   'en': () => require('./en.json'),
// }

/**
 * this can be utilized later on, but now we can rely on fallback language handling missing strings
 * and just debug errors
 */

// export const translate = memoize(
//   (key, config) => i18n.t(key, config).includes('missing') ? key : i18n.t(key, config),
//   (key, config) => (config ? key + JSON.stringify(config) : key),
// );

export const t = memoize(
  (key: keyof typeof en | keyof typeof fi | AuthErrorCodes) =>
    i18n.t(key).includes('missing') ? '' : i18n.t(key)
);

export const init = () => {
  t.cache.clear && t.cache.clear();

  i18n.translations = { en, fi };
  i18n.locale = Localization.locale.includes('fi') ? 'fi' : 'en';
  i18n.fallbacks = true;
};
