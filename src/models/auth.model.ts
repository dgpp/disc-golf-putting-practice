export interface AuthErrorModel {
  message: string;
  code: AuthErrorCodes;
}

export type UserSignInResponseModel =
  | firebase.auth.UserCredential
  | AuthErrorModel
  | undefined;

/**
 * TODO: https://www.wheredidifindthis.com
 */
export enum AuthErrorCodes {
  WrongPassword = 'auth/wrong-password',
  UserNotFound = 'auth/user-not-found',
  InvalidEmail = 'auth/invalid-email',
  UserDisabled = 'auth/user-disabled',
}

export enum LogInStatuses {
  LoggedOut = 0,
  Pending = 1,
  LoggedIn = 2,
}
