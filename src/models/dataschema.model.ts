import { TrainingSessionModel } from './training.model';

export interface UserDataSchema {
  sessions: TrainingSessionModel[];
  displayName: string;
  email: string;
}
