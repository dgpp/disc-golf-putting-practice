export interface PuttModel {
  id: number;
  hit: boolean;
  range: number;
  wind: number;
  windDirection: WindDirections;
  puttingStyle: PuttingStyles;
}

export enum PuttingStyles {
  Staggered = 0,
  Straddle = 1,
  JumpPutt = 2,
  StepPutt = 3,
  OnKnee = 4,
}

export enum WindDirections {
  HeadWind = 0,
  CrossRight = 1,
  TailWind = 2,
  CrossLeft = 3,
}

export interface TrainingSessionModel {
  id: number;
  duration: TrainingDurationModel;
  putts: PuttModel[];
  date: number;
}

export interface TrainingDurationModel {
  hours: number;
  minutes: number;
  seconds: number;
}
