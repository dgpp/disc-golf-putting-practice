import React, { useEffect } from 'react';
import DashboardScreen from 'features/dashboard/DashboardScreen';
import TrainingListScreen from 'features/training/TrainingListScreen';
import ProfileScreen from 'features/profile/ProfileScreen';
import { Screens } from 'constants/screens';
import { Icon } from 'react-native-elements';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { t } from 'src/localization';
import { themePalette } from 'src/constants/theme';
import { getOptionsWithTitle } from './NavOptions';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from 'src/redux';
import { readSessions, readUserData } from 'src/api/database';
import { setTrainingList } from 'src/features/training/trainingSlice';
import { createStackNavigator } from '@react-navigation/stack';
import SessionScreen from 'src/features/training/SessionScreen';
import SessionDetailsScreen from 'src/features/training/SessionDetailsScreen';
import ProfileEditScreen from 'src/features/profile/ProfileEditScreen';
import PuttListScreen from 'src/features/training/PuttListScreen';
import PuttDetails from 'src/features/training/PuttDetailsScreen';
import PuttDetailsScreen from 'src/features/training/PuttDetailsScreen';
import { setDisplayName, setEmail } from 'src/features/profile/profileSlice';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

function AppNav(props: any) {
  const { userId } = useSelector((state: RootState) => state.auth);
  const dispatch = useAppDispatch();
  useEffect(() => {
    const getSessions = async () => {
      // const sessions = await readSessions(userId);
      const res = await readUserData(userId);
      if (res) {
        dispatch(setDisplayName(res.displayName));
        dispatch(setEmail(res.email));
        dispatch(setTrainingList(res.sessions ? res.sessions : []));
      }
    };

    getSessions();
  }, []);
  return (
    <Tab.Navigator shifting={true} initialRouteName={Screens.dashboard}>
      <Tab.Screen
        name={Screens.dashboard}
        options={{
          tabBarIcon: (props) => <Icon name='insert-chart' {...props} />,
          tabBarLabel: t('nav/dashboard'),
          tabBarColor: themePalette.backgroundPrimary,
          title: t('nav/dashboard'),
        }}
        component={DashboardScreen}
      />
      <Tab.Screen
        name={Screens.trainingList}
        options={{
          tabBarIcon: (props) => <Icon name='list' {...props} />,
          tabBarLabel: t('nav/training-list'),
          tabBarColor: themePalette.backgroundPrimary,
          title: t('nav/training-list'),
          ...getOptionsWithTitle(t('nav/training-list')),
        }}
        component={TrainingListScreen}
      />
      <Tab.Screen
        name={Screens.profile}
        options={{
          tabBarIcon: (props) => <Icon name='group' {...props} />,
          tabBarLabel: t('nav/profile'),
          tabBarColor: themePalette.backgroundPrimary,
          title: t('nav/profile'),
        }}
        component={ProfileScreen}
      />
    </Tab.Navigator>
  );
}

export function AppStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={Screens.app}
        component={AppNav}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={Screens.session}
        component={SessionScreen}
        options={getOptionsWithTitle(t('nav/session'))}
      />
      <Stack.Screen
        name={Screens.profileEdit}
        component={ProfileEditScreen}
        options={getOptionsWithTitle(t('nav/edit-profile'))}
      />
      <Stack.Screen
        name={Screens.sessionDetails}
        component={SessionDetailsScreen}
        options={getOptionsWithTitle(t('nav/session-details'))}
      />
      <Stack.Screen
        name={Screens.puttList}
        component={PuttListScreen}
        options={getOptionsWithTitle(t('nav/putt-list'))}
      />
      <Stack.Screen
        name={Screens.puttDetails}
        component={PuttDetailsScreen}
        options={getOptionsWithTitle(t('nav/putt-details'))}
      />
    </Stack.Navigator>
  );
}

export default AppNav;
