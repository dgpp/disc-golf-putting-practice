import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Screens } from 'constants/screens';
import { t } from 'src/localization';

import LogInScreen from 'features/authentication/LogInScreen';
import SignInScreen from 'features/authentication/SignInScreen';
import AuthenticationScreen from 'features/authentication/AuthenticationScreen';
import { getOptionsWithTitle } from './NavOptions';

const Stack = createStackNavigator();

export function AuthNav() {
  return (
    <Stack.Navigator initialRouteName={Screens.authentication}>
      <Stack.Screen
        name={Screens.authentication}
        component={AuthenticationScreen}
        options={getOptionsWithTitle(t('nav/authentication'))}
      />
      <Stack.Screen
        name={Screens.logIn}
        component={LogInScreen}
        options={getOptionsWithTitle(t('nav/log-in'))}
      />
      <Stack.Screen
        name={Screens.signIn}
        component={SignInScreen}
        options={getOptionsWithTitle(t('nav/sign-in'))}
      />
    </Stack.Navigator>
  );
}

export default AuthNav;
