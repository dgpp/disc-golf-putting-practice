import * as React from 'react';
import { StackNavigationOptions } from '@react-navigation/stack';
import { Icon } from 'react-native-elements';
import { themePalette } from 'src/constants/theme';

export const getOptionsWithTitle = (title: string): StackNavigationOptions => ({
  headerStyle: { backgroundColor: themePalette.backgroundPrimary },
  headerTitleStyle: { color: 'white' },
  headerTitle: title,
  headerBackImage: () => <Icon name='arrow-back' color='white' />,
});
