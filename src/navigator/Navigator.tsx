import * as React from 'react';
import { RootState } from 'src/redux';
import { useSelector } from 'react-redux';
import { AppStack } from './AppStack';
import AuthStack from './AuthStack';
import { NavigationContainer } from '@react-navigation/native';
import { Screens } from 'src/constants/screens';
import { createStackNavigator } from '@react-navigation/stack';
import { LogInStatuses } from 'src/models/auth.model';

const Stack = createStackNavigator();

const Navigator = () => {
  const { logInStatus } = useSelector((state: RootState) => state.auth);

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode='none'>
        {(logInStatus !== LogInStatuses.LoggedIn && (
          <Stack.Screen name={Screens.auth} component={AuthStack} />
        )) || <Stack.Screen name={Screens.app} component={AppStack} />}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
