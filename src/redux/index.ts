import { createStore, combineReducers } from '@reduxjs/toolkit';
import AuthReducer, { IAuthSlice } from 'features/authentication/authSlice';
import ProfileReducer from 'features/profile/profileSlice';
import TrainingReducer from 'features/training/trainingSlice';

import { useDispatch } from 'react-redux';

const allReducers = {
  auth: AuthReducer,
  training: TrainingReducer,
  profile: ProfileReducer,
};

const rootReducer = combineReducers(allReducers);
export type RootState = ReturnType<typeof rootReducer>;

const store = createStore(rootReducer);

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();

export default store;
